import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import sun.security.util.Cache;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class TestGoogle {

    private WebDriver chrome;

    @BeforeMethod
    public void beforeTest() {
        System.setProperty("webdriver.chrome.driver", "c:/Users/sten/Downloads/chromedriver_win32/chromedriver.exe");
        chrome = new ChromeDriver();
    }

    @AfterMethod
    public void afterTest() {
        chrome.close();
        chrome.quit();
    }

    @Test
    public void testGoogle() {

        chrome.navigate().to("http://google.pl");
        assertEquals("aa","aa");
        assertTrue(true);

        WebElement webElement = chrome.findElement(By.name("btnK"));

        assertEquals(webElement.getAttribute("value"), "Szukaj w Google"); // TEST Widoku  Sprawdza czy istnieje element na stronie

        chrome.findElement(By.name("q")).sendKeys("kurs nbp"); // wyszukujemy kurs nbp

        chrome.findElement(By.name("q")).submit(); // wciskamy enter

    }

}
